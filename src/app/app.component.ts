import { Component } from '@angular/core'; 
import {  MqttSocketService } from './services/mqttsocket/mqttsocket.service';
import { NodeService } from './services/node-service/node-service.service';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  providers: [
    MqttSocketService,
    NodeService
  ]
})

export class AppComponent {
  constructor() {
  }
}
