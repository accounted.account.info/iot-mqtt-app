import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MessageModel } from 'src/app/models/message-model';
import { NodeModel } from 'src/app/models/node-model';
import { Message, MqttSocketService } from '../mqttsocket/mqttsocket.service';

@Injectable({
  providedIn: 'root'
})
export class NodeService {

  received: Message[] = [];
  sent: Message[] = [];
  nodes: NodeModel[] = [];
  readonly nodes$ = new BehaviorSubject<NodeModel[]>([]);

  constructor(private WebsocketService: MqttSocketService) {
    WebsocketService.messages.subscribe((msg: Message) => {
      this.received=[msg];
      this.getMsg(msg);      
    });
  }

  getMsg(msg: Message) {
    const data = msg.source.split('/');
    if (data[1] === 'node' && data[2] === 'data'){
      const fromJson = JSON.parse(msg.content);
      console.log("Response from websocket: ", fromJson);
      const node: NodeModel = fromJson;
      this.saveNode(node);
    } else if (data[1] === 'led' && data[2] === 'status'){
      console.log("status from websocket: ", msg.content);
      this.saveNodeStatus(msg.content,data[0]);
    } 
  }

  saveNode(newNode: NodeModel) {
    const result = this.nodes.findIndex( node => {
      return newNode.id === node.id
    })
    if (result!==-1) {
      this.nodes[result] = newNode;
    } else {
      this.nodes.push(newNode);
    }
    this.nodes$.next(this.nodes);
  }

  saveNodeStatus(status:string, id: string){
    const result = this.nodes.findIndex( node => {
      return id === node.id
    })
    if (result!==-1) {
      this.nodes[result].status = status;
    } 
    this.nodes$.next(this.nodes);
  }

  sendMsg(formatedMessage: MessageModel) {
    let message = {
      source: '',
      content: ''
    };
    message.source = `${formatedMessage.id}/led/state`;
    message.content = formatedMessage.state;

    this.sent=[message];
    this.WebsocketService.messages.next(message);
  }
}
