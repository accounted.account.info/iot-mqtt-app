import { Injectable } from '@angular/core';
import { map, Observable, Observer } from 'rxjs';
import { AnonymousSubject, Subject } from 'rxjs/internal/Subject';
import { UrlProviderService } from '../url-provider/url-provider.service';
import {webSocket, WebSocketSubject} from 'rxjs/webSocket';

export interface Message {
  source: string;
  content: string;
}


@Injectable({
  providedIn: 'root'
})
export class MqttSocketService {
  private endpoint = 'mqttSocket';
  private subject!: AnonymousSubject<MessageEvent>;
  public messages: Subject<Message>;

  constructor(readonly urlProviderService: UrlProviderService) {
    this.messages = <Subject<Message>>this.connect(
        this.urlProviderService.getUrl(this.endpoint)
        )
        .pipe(
            map(
                (response: MessageEvent): Message => {
                    console.log(response.data);
                    let data = JSON.parse(response.data)
                    return data;
                }
            )
    );
  }

  public connect(url: string): AnonymousSubject<MessageEvent> {
        if (!this.subject) {
            this.subject = this.create(url);
            console.log("Successfully connected: " + url);
        }
        return this.subject;
  }

  private create(url: string): AnonymousSubject<MessageEvent> {
        let ws = new WebSocket(url);
        let observable = new Observable((obs: Observer<MessageEvent>) => {
            ws.onmessage = obs.next.bind(obs);
            ws.onerror = obs.error.bind(obs);
            ws.onclose = obs.complete.bind(obs);
            return ws.close.bind(ws);
        });
        let observer: Observer<MessageEvent<any>> = {
            error: () => {},
            complete: () => {},
            next: (data: Object) => {
                console.log('Message sent to websocket: ', data);
                if (ws.readyState === WebSocket.OPEN) {
                    ws.send(JSON.stringify(data));
                }
            }
        };
        return new AnonymousSubject<MessageEvent>(observer, observable);
    }

}
