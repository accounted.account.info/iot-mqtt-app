type EndPointsList = {[key: string]: any};

export const EndPoints: EndPointsList = {
    mqttSocket: 'ws://localhost:8999/'
}