import { Injectable } from '@angular/core';
import { EndPoints } from './url-list';


@Injectable({
  providedIn: 'root'
})
export class UrlProviderService {

  private endpoints = EndPoints;
  constructor() { }
  
  getUrl(endpoint: string): string {
    return this.endpoints[endpoint]
  }

}
