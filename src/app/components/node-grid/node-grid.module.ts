import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NodeGridComponent } from './node-grid.component';
import { NodeModule } from './node/node.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [
    NodeGridComponent
  ],
  imports: [
    CommonModule,
    NodeModule,
    BrowserAnimationsModule
  ],
  exports: [
    NodeGridComponent
  ]
})
export class NodeGridModule { }
