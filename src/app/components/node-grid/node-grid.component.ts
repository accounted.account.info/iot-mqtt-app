import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { NodeModel } from 'src/app/models/node-model';
import { NodeService } from 'src/app/services/node-service/node-service.service';

@Component({
  selector: 'app-node-grid',
  templateUrl: './node-grid.component.html',
  styleUrls: ['./node-grid.component.scss']
})
export class NodeGridComponent implements OnInit {

  title: string = 'socketrv';
  content: string = 'hello';
  source: string = 'angular'; 
  nodes$ = new BehaviorSubject<NodeModel[]>([]);

  constructor(readonly nodeService: NodeService) { }

  ngOnInit(): void {
    this.nodes$ = this.nodeService.nodes$
  }

}
