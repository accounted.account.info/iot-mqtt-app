import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MessageModel } from 'src/app/models/message-model';
import { NodeModel } from 'src/app/models/node-model';
import { NodeService } from 'src/app/services/node-service/node-service.service';

@Component({
  selector: 'app-node',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.scss']
})
export class NodeComponent implements OnInit {

  @Input('node') node!: NodeModel;
  
  constructor( readonly nodeService : NodeService) { }

  ngOnInit(): void {
  }

  sendMsg(id: string ,state: string) {
    this.nodeService.sendMsg({
      id: id,
      state: state
    });
  }
}
