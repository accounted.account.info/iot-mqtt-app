export interface NodeModel {
    id: string,
    name: string,
    type: string,
    states: string[],
    status?: string
}
